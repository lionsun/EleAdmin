/**
 * 事件总线
 */
const EventBus = {
  events: {},
  emit(event, ...args) {
    const Listeners = this.events[event];
    if (!Listeners) return;
    Listeners.forEach((e) => { e(...args); });
  },
  on(event, listener) {
    this.events = this.events || {};
    if (!this.events[event]) this.events[event] = [];
    this.events[event].unshift(listener);
  },
  stack(event, ...args) {
    const Listeners = this.events[event];
    if (!Listeners) return;
    Listeners.some(e => e(...args));
  },
};

export default EventBus;
